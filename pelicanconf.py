#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# Generally needed or desirable
SITENAME = "Dumb Notepad"
SITEURL = ""
THEME ='dumb-main'
CSS_FILE = 'main.css'

# Remove or change
AUTHOR = "Namey Nameson"
DEFAULT_DATE_FORMAT = '%d-%m-%Y'
DEFAULT_PAGINATION = 6
FAVICON = 'images/favicon.png'
NAV_INDEX = {
        # Additional navigation items. {'name': 'link'}
        'about-page': 'p/about',
        'src-url': 'https://gitlab.com/iivxx/dumb',
        'feed': 'atom.xml'
        }
OUTPUT_PATH='public'
PATH = 'works'
SITESUBTITLE = "A Dumb Notepad For Single Players"

# 'summary.html' tries to produce summaries even if they're absent.
# 'False' =  suppress.
SUMMARIES=True
SUMMARY_MAX_LENGTH=None

# Shorten stuff
CATEGORY_URL = 'c/{slug}.html'
CATEGORY_SAVE_AS = 'c/{slug}.html'
DEFAULT_CATEGORY = 'unlabeled'
DISPLAY_CATEGORIES_ON_MENU = None
PAGE_URL = 'p/{slug}.html'
PAGE_SAVE_AS = 'p/{slug}.html'
TAG_URL = 't/{slug}.html'
TAG_SAVE_AS = 't/{slug}.html'

FEED_ALL_ATOM = 'atom.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Not needed
AUTHOR_SAVE_AS=''
ARCHIVES_SAVE_AS = ''

# Uncomment following line if you want document-relative URLs when developing.
RELATIVE_URLS = False
PORT = 4000
