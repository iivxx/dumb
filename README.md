### Notes Regarding 'Dumb Notepad':

* Plain Notepad is meant to be a single author theme first.
* As the name suggest it tries to be _plain and simple_. It is text-oriented
  and purposely minimalistic.  
    - Since the users needs use Markdown leave most the formatting to them.
    - Doesn't try to cover _every_ use case.
    - Short CSS-file and HTML-oriented.
    - It's based on and even more basic than [my other Pelican theme][other].
    - Think [bettermotherfuckingwebsite.com][bmf].
    - It _tries_ to be easily configurable.
* '`filter.html`' is used to produce stuff like Wikipedia-style references and
  indented paragraphs.
    - The current [trigger characters][trig]: <code>'&micro;&micro;'</code>,
      <code>'&#94;&#94;'</code> plus <code>'$&#94;'</code>
    - `jinja`-filters are used. No plugins.
* Categories and tags are used.
* Theme-specific `pelicanconf.py` variables. See file sample.
* You might also want to look at an [Example Post](/usage) for usage.

See [Pelican docs](https://docs.getpelican.com/en/4.0.1/) for
configuration specifics.  
Source: [Dumb Notepad @ Gitlab](https://gitlab.com/iivxx/dumb)  
License: [Mozilla Public License 2.0](https://www.mozilla.org/en-US/MPL/2.0/)

[bmf]: http://bettermotherfuckingwebsite.com
[other]: https://gitlab.com/iivxx/plain-notepad
[trig]: /usage#micro
